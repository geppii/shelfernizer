@extends('layout.app')
@section('content')
@include('partitials.new-shelf-modal')
@include('partitials.delete-shelf-modal')
@include('partitials.stuff-modal')
    <div class="box">
        <?php
        /** @var $shelf \App\Models\Shelf */
        ?>
        @foreach($shelves as $shelf)
            <div class="shelfcontainer">
                <div class="shelfheader">
                    <div class="shelfname">
                        {{$shelf->name}}
                    </div>
                    <div class="shelfoptions">
                        <div data-delete-route="{{route('shelf.delete',$shelf->id)}}" data-bs-toggle="modal" data-bs-target="#delete-shelf-modal" class="shelf-delete bi bi-trash"></div>
                    </div>
                </div>
                <div data-shelf-id="{{$shelf->id}}" data-shelf-name="{{$shelf->name}}"  class="shelf">
                    @for($i=1;$i <= $shelf->shelftype->width; $i++)

                        <div class="shelfrow">
                            @for($j=1;$j<= $shelf->shelftype->height; $j++)
                                <?php
                                if(isset($shelf->trays()[$i][$j])){
                                   $id = $shelf->trays()[$i][$j]->id;
                                }else{
                                    $id = 0;
                                }
                                ?>
                                <div id="tray-{{$id}}" data-tray-id="{{$id}}" class="tray traytype-box">

                                </div>
                            @endfor
                        </div>

                    @endfor

                </div>
            </div>

        @endforeach
    </div>

@endsection

