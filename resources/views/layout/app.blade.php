<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <title>Shelfernizer</title>
</head>
<body>


@yield('modal')
@yield('modal-del')
@yield('modal-stuff')
@include('layout.navbar')

@yield('content')


<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/shelf.js') }}"></script>
<script src="{{ asset('js/tags.js') }}"></script>

</body>
</html>
