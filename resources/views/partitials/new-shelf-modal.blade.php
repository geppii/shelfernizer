@section('modal')

    <div id="new-shelf-modal" class="modal" tabindex="-1">
        <form action="/" method=POST>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Neues Regal</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @csrf

                        @if (isset($method) and $method == 'PUT')
                            @method('PUT')
                        @endif

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Name:</strong>
                                    <input type="text" name="shelf-name" class="form-control" placeholder="Bezeichnung"
                                           value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Regaltyp:</strong>
                                    <select class="form-control" name="shelf-type-id">
                                        @foreach($shelfTypes as $typ)
                                            <option value="{{ $typ->id }}">
                                                {{ $typ->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Ort:</strong>
                                    <input type="text" name="shelf-location" class="form-control"
                                           placeholder="Aufstellungsort" value="">
                                </div>

                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
                        <button type="submit" class="btn btn-primary">Speichern</button>
                    </div>
                </div>
            </div>
        </form>
    </div>



@endsection
