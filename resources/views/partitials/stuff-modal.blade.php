@section('modal-stuff')

    <div id="stuff-modal" class="modal" tabindex="-1">
        <form id="stuff-form" action="" method=POST>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Stuff</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @csrf

                        @if (isset($method) and $method == 'PUT')
                            @method('PUT')
                        @endif

                        <div class="row">


                            <div class="stuff-select col-xs-12 col-sm-12 col-md-12">


                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
                        <button type="submit" class="btn btn-primary">Speichern</button>
                    </div>
                </div>
            </div>
        </form>
    </div>



@endsection
