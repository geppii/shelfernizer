@section('modal-del')

    <div id="delete-shelf-modal" class="modal" tabindex="-1">
        <form id="delete-shelf-form" action="./" method=POST>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Regal löschen</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        @method('delete')


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div id="shelf-del-warning" data-placeholder="Das Regal $1 wird unwiderruflich gelöscht. "class="alert-danger"></div>
                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
                        <button type="submit" class="btn btn-primary">Löschen</button>
                    </div>
                </div>
            </div>
        </form>
    </div>



@endsection
