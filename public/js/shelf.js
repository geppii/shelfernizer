$(document).ready(function () {
    $('.shelfoptions').css('visibility', 'hidden');

    $('.shelfheader').mouseover(function () {
        $(this).find('.shelfoptions').css('visibility', 'visible');
        $(this).find('.shelfname').hide();
    });

    $('.shelfcontainer').mouseout(function () {

        $(this).find('.shelfoptions').css('visibility', 'hidden');
        $(this).find('.shelfname').show();
    });

    $('.shelf-delete').click(function () {
        var shelf = $(this).closest('.shelfcontainer').find('.shelf');
        var text = $('#shelf-del-warning').data('placeholder');
        text = text.replace('$1', $(shelf).data('shelf-name'));

        $('#delete-shelf-form').attr('action', $(this).data('delete-route'))


        $('#shelf-del-warning').html(text)
    });


    $('.tray').click(function () {
        var id = $(this).data('tray-id');
        $('#stuff-modal').modal('show');
        $('#stuff-form').attr('action', '/stuff/add/' + id)


        $.get("/stuff/" + id, function (data) {
            console.log(data);
            $('#stuff-modal').find('.stuff-select').html(data);

            createTagify();
            createDropzone();
        });
    });

    $('#search-input').on('input',function () {
        $('.tray').each(function () {
            $(this).removeClass('tray-found')
        });
        $.get("/stuff/search/" + encodeURIComponent($(this).val()) , function (data) {
            $.each(data, function( index, value ) {
                $('#tray-'+value.trayId).addClass('tray-found');
               console.log(value);
            });

        });
       console.log();
    });

});


function createTagify() {

    var input = document.querySelector('.stuff-input'),
        tagify = new Tagify(input, {
             dropdown : {
                position: 'text',
                enabled: 1 // show suggestions dropdown after 1 typed character
            }
        }),
        button = input.nextElementSibling;  // "add new tag" action-button

    button.addEventListener("click", onAddButtonClick)

    function onAddButtonClick(){
        tagify.addEmptyTag()
    }

    function onInvalidTag(e){
        console.log("invalid", e.detail)
    }
}

function createDropzone() {
    var dropzone = new Dropzone('#demo-upload', {
        previewTemplate: document.querySelector('#preview-template').innerHTML,
        parallelUploads: 2,
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        maxFilesize: 3,
        filesizeBase: 1000,
        init: function () {
            this.on("addedfile", function (file) {

                // Create the remove button
                var removeButton = Dropzone.createElement("<button>Remove file</button>");


                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function (e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });
        },
        thumbnail: function (file, dataUrl) {
            if (file.previewElement) {
                file.previewElement.classList.remove("dz-file-preview");
                var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                for (var i = 0; i < images.length; i++) {
                    var thumbnailElement = images[i];
                    thumbnailElement.alt = file.name;
                    thumbnailElement.src = dataUrl;
                }
                setTimeout(function () {
                    file.previewElement.classList.add("dz-image-preview");
                }, 1);
            }
        }

    });


}


