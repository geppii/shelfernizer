<?php

namespace Database\Seeders;

use App\Models\Shelftype;
use App\Models\Traytype;
use App\Repositories\ShelfTypeRepository;
use App\Repositories\TrayTypeRepository;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    protected ShelfTypeRepository $shelfTypeRepository;
    protected TrayTypeRepository  $trayTypeRepository;
    public function __construct(ShelfTypeRepository $shelfTypeRepository, TrayTypeRepository  $trayTypeRepository){
        $this->shelfTypeRepository = $shelfTypeRepository;
        $this->trayTypeRepository = $trayTypeRepository;
    }


    protected function createKallax($h, $w){
        $this->shelfTypeRepository->create(
            ['name' => 'Kallax '.$h.'x'.$w, 'height' => $h, 'width' => $w, 'image' => storage_path('shelftypes/kallax_'.$h.'x'.$w.'.png')]
        );
    }

    /**
     * Seed the application's database.
     *
     * @return void
     * @throws \JsonException
     */
    public function run()
    {

        $kallax = [[4,4],[1,2],[1,3],[1,4]];

        foreach ($kallax as $k){
            $this->createKallax($k[0], $k[1]);
            if($k[0]!==$k[1]){
                $this->createKallax($k[1], $k[0]);
            }
        }


        $this->trayTypeRepository->create(['name'=>'Box','configuration'=>1]);



    }
}
