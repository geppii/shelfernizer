<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Shelftype extends Migration
{

    public function up()
    {
        Schema::create('shelftypes', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('configuration')->nullable(true);;
            $table->integer('height');
            $table->integer('width');
            $table->text('image')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shelftypes');
    }
}
