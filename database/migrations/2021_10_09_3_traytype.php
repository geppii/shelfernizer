<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Traytype extends Migration
{
    public function up()
    {
        Schema::create('traytypes', function (Blueprint $table) {
            $table->id();
            $table->text('name')->unique();
            $table->text('configuration')->nullable(true);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traytypes');
    }
}
