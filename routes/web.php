<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\Controller::class,'index']);
Route::post('/',[\App\Http\Controllers\Controller::class,'post']);
Route::delete('/shelf/{id}',[\App\Http\Controllers\Controller::class,'delete'])->name('shelf.delete');

Route::get('/settings',[\App\Http\Controllers\SettingsController::class, 'index'])->name('admin.index');
Route::get('/settings/{type}',[\App\Http\Controllers\SettingsController::class, 'index'])->name('admin.indextype');
Route::get('/shelf',[\App\Http\Controllers\SettingsController::class, 'index'])->name('shelf.create');

Route::get('stuff/{trayId}',[\App\Http\Controllers\StuffController::class, 'getHtmlForTray']);

Route::get('stuff/search/{search}',[\App\Http\Controllers\StuffController::class, 'search']);
Route::post('stuff/add/{trayId}',[\App\Http\Controllers\StuffController::class, 'add']);

Route::post('tray/{trayId}/upload',[\App\Http\Controllers\TrayController::class, 'upload']);
