<?php


namespace App\Repositories;



use App\Models\Shelf;
use App\Models\Shelftype;
use Illuminate\Container\Container as App;

class ShelfRepository extends Repository
{


    protected  TrayRepository $trayRepository;
    protected ShelfTypeRepository $shelfTypeRepository;
    protected TrayTypeRepository  $trayTypeRepository;

    public function __construct(App $app,TrayRepository $trayRepository, ShelfTypeRepository $shelfTypeRepository, TrayTypeRepository  $trayTypeRepository)
    {
        $this->trayRepository = $trayRepository;
        $this->shelfTypeRepository =  $shelfTypeRepository;
        $this->trayTypeRepository = $trayTypeRepository;
        parent::__construct($app);
    }


    /**
     * @return string
     */
    public function model()
    {
        return Shelf::class;
    }

    public function create(array $attributes)
    {
        $id =  parent::create($attributes);
        /** @var Shelftype $shelfType */
        $shelfType = $this->shelfTypeRepository->getById($attributes['shelftype_id'])->get()[0];

        if(isset($attributes['traytype_id'])){
            $trayTypeId = $attributes['traytype_id'];
        }else{
            $trayTypeId = $this->trayTypeRepository->getAll()->where('name','=','Box')[0]->id;
        }

        for ($i=1;$i <= $shelfType->width; $i++){
            for($j=1;$j<= $shelfType->height; $j++){
                $this->trayRepository->create( ['x'=>$i, 'y'=>$j, 'traytype_id'=>$trayTypeId, 'shelf_id' => $id]);
            }
        }
    }

}

