<?php


namespace App\Repositories;


;

use App\Models\Stuff;
use Illuminate\Container\Container as App;

class StuffRepository extends Repository
{



    public function __construct(App $app)
    {
        parent::__construct($app);
    }


    /**
     * @return string
     */
    public function model()
    {
        return Stuff::class;
    }

    public function getByTrayId($trayId){
        return $this->getAll()->where('tray_id' ,'=', $trayId);
    }


    public function search($search){

        $results = $this->model->where('name','like',  "%$search%");

        $return = [];
        foreach ($results->get() as $result){
            $return[] = ['id'=>$result->id,'trayId' => $result->tray_id,'result'=> $result->name];
        }
        return $return;
    }
}

