<?php


namespace App\Repositories;



use App\Models\Shelf;
use App\Models\Traytype;
use Illuminate\Container\Container as App;

class TrayTypeRepository extends Repository
{



    public function __construct(App $app)
    {
        parent::__construct($app);
    }


    /**
     * @return string
     */
    public function model()
    {
        return Traytype::class;
    }


}

