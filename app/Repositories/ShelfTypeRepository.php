<?php


namespace App\Repositories;



use App\Models\Shelf;
use App\Models\Shelftype;
use Illuminate\Container\Container as App;

class ShelfTypeRepository extends Repository
{



    public function __construct(App $app)
    {
        parent::__construct($app);
    }


    /**
     * @return string
     */
    public function model()
    {
        return Shelftype::class;
    }


}

