<?php

namespace App\Repositories;

/**
 * Interface RepositoryInterface
 *
 * Interface for the Repositories
 *
 * @category Repository
 * @package  App\Repositories\Interfaces
 * @author   Daniel Streit <daniel.streit@de.atu.eu>
 * @license  internal usage
 * @link     -
 */
interface RepositoryInterface
{
    /**
     * Declares the function getAll
     *
     * @return mixed a list of all
     */
    public function getAll();

    /**
     * Declares the function getById
     *
     * @param integer $id the id of the model
     *
     * @return mixed the model
     */
    public function getById($id);

    /**
     * Declares the function create
     *
     * @param array $attributes the attributes to create
     *
     * @return mixed the created model
     */
    public function create(array $attributes);

    /**
     * Declares the function update
     *
     * @param integer $id         the id of the model
     * @param array   $attributes the attributes to update
     *
     * @return integer the effective rows
     */
    public function update($id, array $attributes);

    /**
     * Declares the function delete
     *
     * @param integer $id the model id to delete
     *
     * @return mixed the status of the deletion
     */
    public function delete($id);
}
