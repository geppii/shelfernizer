<?php


namespace App\Repositories;



use App\Models\Shelf;
use App\Models\Shelftype;
use App\Models\Tray;
use Illuminate\Container\Container as App;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class TrayRepository extends Repository
{


    protected TrayImageRepository $trayImageRepository;

    public function __construct(App $app, TrayImageRepository $trayImageRepository)
    {
        $this->trayImageRepository = $trayImageRepository;
        parent::__construct($app);
    }


    /**
     * @return string
     */
    public function model()
    {
        return Tray::class;
    }


    public function uploadImage($trayId, Request $request){

        /** @var UploadedFile $file */
        foreach ($request->allFiles() as $file){
            $filename = $trayId.'/'.Str::uuid().'.'.$file->getClientOriginalName();
            $file->storeAs('/uploads/',$filename);

            $this->trayImageRepository->create( ['tray_id' => $trayId, 'path' => $filename]);
        }
    }

}

