<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 *
 * Implementation of the RepositoryInterface
 *
 * @category Repository
 * @package  App\Repositories
 * @author   Daniel Streit <daniel.streit@de.atu.eu>
 * @license  internal usage
 * @link     -
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var Model $model
     */
    protected $model;

    /**
     * @var \Illuminate\Container\Container
     */
    private $app;

    private $softDeleteModel = false;

    /**
     * Repository constructor.
     *
     * @param \Illuminate\Container\Container $app the app container
     *
     * @return void
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Makes the model declaration
     *
     * @return mixed the model class
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        $this->model = $model;

        if(property_exists($this->model,'deleted')){
            $this->softDeleteModel = true;
        }

        return $this->model;
    }

    /**
     * Sets the model object to the repository
     *
     * @return mixed the model to define
     */
    abstract public function model();

    /**
     * Returns all model representations from the database
     *
     * @codeCoverageIgnore
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public function getAll()
    {
        $return =  $this->model::all();
        if($this->softDeleteModel){
            $return->where('deleted', '=', false);
        }
        return $return;
    }


    /**
     * Gets the model by its id from the database
     *
     * @param int $id the id of the model
     *
     * @return mixed
     */
    public function getById($id)
    {
        $return = $this->model->where($this->model->getKeyName(), '=', $id);
        if($this->softDeleteModel){
            $return->where('deleted', '=', false);
        }
        return $return;
    }

    /**
     * Creates the model with the given attributes
     *
     * @param array $attributes the attributes to create
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes)->getKey();
    }

    /**
     * Updates the model with the given attributes for the given id
     *
     * @param int   $id         the id of the model
     * @param array $attributes the attributes to update
     *
     * @return int
     */
    public function update($id, array $attributes)
    {
        $return = $this->model->where($this->model->getKeyName(), '=', $id);

        if($this->softDeleteModel){
            $return->where('deleted', '=', false);
        }
        return $return->update($attributes);
    }

    /**
     * Deltes the model with the given id
     *
     * @param int $id the model id to delete
     *
     * @codeCoverageIgnore
     *
     * @return mixed|void
     */
    public function delete($id)
    {
        if($this->softDeleteModel){
            $attributes = ['deleted' => true];
            $this->update($id, $attributes);
        }else{
            $this->model->destroy($id);
        }

        return true;

    }
}
