<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $tray_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property Tray $tray
 */
class Stuff extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['tray_id', 'name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tray()
    {
        return $this->belongsTo('App\Models\Tray','tray_id','id','trays');
    }
}
