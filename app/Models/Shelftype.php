<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $configuration
 * @property int $height
 * @property int $width
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 */
class Shelftype extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'configuration', 'height', 'width', 'image', 'created_at', 'updated_at'];

}
