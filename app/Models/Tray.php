<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $x
 * @property int $y
 * @property int $traytype_id
 * @property int $shelf_id
 * @property string $created_at
 * @property string $updated_at
 * @property Shelf $shelf
 * @property Traytype $traytype
 */
class Tray extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['x', 'y', 'traytype_id', 'shelf_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shelf()
    {
        return $this->belongsTo('App\Models\Shelf');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function traytype()
    {
        return $this->belongsTo('App\Models\Traytype');
    }

    public function images(){
        return $this->hasMany(\TrayImages::class, 'tray_id','id');
    }
}
