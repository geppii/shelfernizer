<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $configuration
 * @property string $created_at
 * @property string $updated_at
 */
class Traytype extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'configuration', 'created_at', 'updated_at'];

}
