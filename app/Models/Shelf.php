<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $location
 * @property int $shelftype_id
 * @property string $created_at
 * @property string $updated_at
 * @property Shelftype $shelftype
 */
class Shelf extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['name', 'location', 'shelftype_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shelftype()
    {
        return $this->belongsTo('App\Models\Shelftype');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trays()
    {
        $return = [];
        $trays = $this->hasMany('App\Models\Tray','shelf_id','id')->get();
        foreach ($trays as $tray){
            $return[$tray->x][$tray->y] = $tray;
        }
        return $return;
    }
}
