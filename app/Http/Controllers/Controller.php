<?php

namespace App\Http\Controllers;

use App\Repositories\ShelfRepository;
use App\Repositories\ShelfTypeRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected ShelfTypeRepository $shelfTypeRepository;
    protected ShelfRepository $shelfRepository;
    public function __construct(ShelfRepository $shelfRepository, ShelfTypeRepository $shelfTypeRepository)
    {
        $this->shelfRepository = $shelfRepository;
        $this->shelfTypeRepository = $shelfTypeRepository;
    }

    public function index(){
        $shelves = $this->shelfRepository->getAll();
        $shelfTypes = $this->shelfTypeRepository->getAll();

        return view('index',compact(['shelves','shelfTypes']));
    }

    public function post(Request $request){
        if($request->get('shelf-name')){
            $this->shelfRepository->create(
                ['name' => $request->get('shelf-name'),
                'location' => $request->get('shelf-location'),
                'shelftype_id' => $request->get('shelf-type-id')
                ]
            );
        }

        return $this->index();

    }

    public function delete($id){

        $this->shelfRepository->delete($id);
        return $this->index();
    }
}
