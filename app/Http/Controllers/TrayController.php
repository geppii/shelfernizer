<?php

namespace App\Http\Controllers;

use App\Repositories\ShelfRepository;
use App\Repositories\ShelfTypeRepository;
use App\Repositories\StuffRepository;
use App\Repositories\TrayRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class TrayController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected ShelfTypeRepository $shelfTypeRepository;
    protected ShelfRepository $shelfRepository;
    protected TrayRepository $trayRepository;
    public function __construct(TrayRepository $trayRepository)
    {
        $this->trayRepository = $trayRepository;
    }

    public function upload($trayId, Request $request){

        $stuff = $this->trayRepository->uploadImage($trayId, $request);

        return true;

    }

    public function delete($id){

        $this->shelfRepository->delete($id);
        return $this->index();
    }
}
