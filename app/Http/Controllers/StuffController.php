<?php

namespace App\Http\Controllers;

use App\Repositories\ShelfRepository;
use App\Repositories\ShelfTypeRepository;
use App\Repositories\StuffRepository;
use App\Repositories\TrayRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class StuffController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected ShelfTypeRepository $shelfTypeRepository;
    protected ShelfRepository $shelfRepository;
    protected StuffRepository $stuffRepository;
    protected TrayRepository $trayRepository;

    public function __construct(StuffRepository $stuffRepository, TrayRepository $trayRepository)
    {
        $this->stuffRepository = $stuffRepository;
        $this->trayRepository = $trayRepository;
    }

    public function getHtmlForTray($trayId)
    {
        $tray = $this->trayRepository->getById($trayId)->get()[0];
        $stuff = $this->stuffRepository->getByTrayId($trayId);

        return view('stuffselect', compact('tray', 'stuff'));
    }

    public function search($search){
        $stuff = $this->stuffRepository->search($search);


        return $stuff;
    }




    public function add($trayId, Request $request)
    {

        if ($request->get('stuff-input')) {
            $values = json_decode($request->get('stuff-input'));


            $names = [];
            foreach ($values as $value) {
                $names[] = $value->value;
                $this->stuffRepository->create(
                    [
                        'name' =>  $value->value,
                        'tray_id' => $trayId
                    ]
                );
            }
            $deletable = $this->stuffRepository->getAll()->whereNotIn('name',$names)->where('tray_id','=',$trayId);

            foreach ($deletable->all() as $item){
                $item->delete();
            }

        }

        return redirect('/');

    }

    public function delete($id)
    {

        $this->shelfRepository->delete($id);
        return $this->index();
    }
}
